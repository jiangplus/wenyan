require 'test_helper'

class PostsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get check" do
    get :check
    assert_response :success
  end

  test "should get result" do
    get :result
    assert_response :success
  end

  test "should get users" do
    get :users
    assert_response :success
  end

end
