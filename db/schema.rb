# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130503021508) do

  create_table "articles", :force => true do |t|
    t.string   "title"
    t.integer  "author_id"
    t.integer  "textbook_id"
    t.integer  "term_id"
    t.text     "info"
    t.text     "content"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "subtitle"
  end

  create_table "authors", :force => true do |t|
    t.string   "name"
    t.text     "detail"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "exams", :force => true do |t|
    t.integer  "pass_count",  :default => 0
    t.integer  "fail_count",  :default => 0
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.integer  "sentence_id"
  end

  create_table "sentences", :force => true do |t|
    t.integer  "article_id"
    t.text     "content"
    t.text     "translation"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "terms", :force => true do |t|
    t.string   "title"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "textbooks", :force => true do |t|
    t.string   "title"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "username"
    t.string   "location"
    t.string   "textbook"
    t.string   "imei",         :default => ""
    t.string   "os",           :default => ""
    t.string   "os_version",   :default => ""
    t.integer  "resolution_x", :default => 0
    t.integer  "resolution_y", :default => 0
    t.datetime "install_time"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.string   "token"
  end

  create_table "words", :force => true do |t|
    t.integer  "sentence_id"
    t.string   "kind"
    t.string   "content"
    t.string   "translation"
    t.string   "mistakes"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

end
