class CreateSentences < ActiveRecord::Migration
  def change
    create_table :sentences do |t|
      t.integer :id
      t.integer :article_id
      t.text :content
      t.text :translation

      t.timestamps
    end
  end
end
