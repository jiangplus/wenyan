class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.integer :id
      t.string :email
      t.string :username
      t.string :location
      t.string :textbook

      t.string    :imei, :default => ''
      t.string    :os, :default => ''
      t.string    :os_version, :default => ''
      t.integer   :resolution_x, :default => 0
      t.integer   :resolution_y, :default => 0
      t.datetime  :install_time

      t.timestamps
    end
  end
end
