class CreateWords < ActiveRecord::Migration
  def change
    create_table :words do |t|
      t.integer :id
      t.integer :sentence_id
      t.string :kind
      t.string :content
      t.string :translation
      t.string :mistakes

      t.timestamps
    end
  end
end
