class CreateTextbooks < ActiveRecord::Migration
  def change
    create_table :textbooks do |t|
      t.integer :id
      t.string :title

      t.timestamps
    end
  end
end
