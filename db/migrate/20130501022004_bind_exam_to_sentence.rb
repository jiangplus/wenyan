class BindExamToSentence < ActiveRecord::Migration
  def up
  	remove_column :exams, :word_id
  	add_column :exams, :sentence_id, :integer
  end

  def down
  end
end
