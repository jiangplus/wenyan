class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.integer :id
      t.string :title
      t.integer :author_id
      t.integer :textbook_id
      t.integer :term_id
      t.text :info
      t.text :content

      t.timestamps
    end
  end
end
