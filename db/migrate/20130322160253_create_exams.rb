class CreateExams < ActiveRecord::Migration
  def change
    create_table :exams do |t|
      t.integer :id
      t.integer :word_id
      t.integer :pass_count, :default => 0
      t.integer :fail_count, :default => 0

      t.timestamps
    end
  end
end
