class PostsController < ApplicationController
  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new

    if request.post?
    # render :text => params

    @article = Article.new(params[:article])
    @article.save

    params[:sentences].each do |s|
      sentence = Sentence.new :content => s[:content], :translation => s[:translation]
      sentence.article_id = @article.id
      sentence.save
      if  s[:words]
        s[:words].each do |w|
          word = Word.new(:content => w[:content], :translation => w[:translation], :kind => w[:kind], :mistakes => w[:mistakes])
          word.sentence_id = sentence.id
          word.save
        end
      end
    end

    redirect_to posts_show_path(@article)

    end
  end

  def remove
    article = Article.find(params[:id])
    article.delete()
    render :text => :ok
  end


  def users
    @users = User.all
  end

  def list_authors
    @authors = Author.all
  end

  def add_author
    author = Author.create(:name => params[:name],:detail => params[:detail])
    render :text => author.id
  end

  def remove_author
    author = Author.find(params[:id])
    author.delete()
    render :text => :ok
  end


  def list_textbooks
    @textbooks = Textbook.all
  end

  def add_textbook
    textbook = Textbook.create(:title => params[:name])
    render :text => textbook.id
  end

  def remove_textbook
    textbook = Textbook.find(params[:id])
    textbook.delete()
    render :text => :ok
  end


  def list_terms
    @terms = Term.all
  end

  def add_term
    term = Term.create(:title => params[:name])
    render :text => term.id
  end

  def remove_term
    term = Term.find(params[:id])
    term.delete()
    render :text => :ok
  end

  def api_intro
  end

  # actions for api

  def new_user
    @user = User.create(params[:user])
    @user.token = rand.to_s
    @user.save
    render :text => @user.token
  end

  def update_list
    @articles = Article.all

    render :json => @articles.to_json(
      :except => [:author_id, :term_id, :textbook_id, :content],
      :include => {
        :author =>{:only => [:id, :name, :detail]}, 
        :term => {:only => [:id, :title]}, 
        :textbook => {:only => [:id, :title]}})
  end

  def update_article
    @article = Article.find(params[:id])

    render :json => @article.to_json(
      :except => [:author_id, :term_id, :textbook_id],
      :include => {
        :sentences => {:include => :words},
        :author =>{:only => [:id, :name, :detail]}, 
        :term => {:only => [:id, :title]}, 
        :textbook => {:only => [:id, :title]},
        :words =>  { :except => []} 
        })
  end

  def answer_check
    @exam = Exam.where(:sentence_id => params[:sentence_id]).first_or_create
    @exam.fail_count += 1
    @exam.save
    render :text => '{"result" : true}'
  end

  def answer_stats
    @exams = Exam.all
    render json: @exams
  end

end
