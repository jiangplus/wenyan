class Word < ActiveRecord::Base
  attr_accessible :content, :id, :kind, :mistakes, :sentence_id, :translation
  belongs_to :article
  belongs_to :sentence
end
