class Term < ActiveRecord::Base
  attr_accessible :id, :title
  has_many :articles
end
