class Article < ActiveRecord::Base
  attr_accessible :author_id, :content, :id, :info, :term_id, :textbook_id, :title, :subtitle
  belongs_to :author
  belongs_to :textbook
  belongs_to :term
  has_many :sentences
  has_many :words, :through => :sentences
end
