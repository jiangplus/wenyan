class User < ActiveRecord::Base
  attr_accessible :email, :id, :location, :textbook, :username, :install_time, :imei, :os, :os_version, :resolution_x, :resolution_y, :user, :token
end
