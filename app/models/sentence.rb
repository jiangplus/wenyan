class Sentence < ActiveRecord::Base
  attr_accessible :article_id, :content, :id, :translation
  belongs_to :article
  has_many :words
  has_one :exam
end
