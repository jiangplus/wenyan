class Exam < ActiveRecord::Base
  attr_accessible :fail_count, :id, :pass_count, :sentence_id
  belongs_to :sentence
end
