class Author < ActiveRecord::Base
  attr_accessible :detail, :id, :name
  has_many :articles
end
