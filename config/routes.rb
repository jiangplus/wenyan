Wenyan::Application.routes.draw do
  root :to => "posts#index"
  get "posts/index"

  match "posts/show/:id" => "posts#show" , :as => :posts_show

  match "posts/new"

  match "posts/remove"

  match "posts/check"

  match "posts/result"

  get "posts/users"

  get "posts/list_authors"

  post "posts/add_author"

  post "posts/remove_author"

  get "posts/list_textbooks"

  post "posts/add_textbook"

  post "posts/remove_textbook"

  get "posts/list_terms"

  post "posts/add_term"

  post "posts/remove_term"

  match "new_user" => "posts#new_user", :as => :new_user

  get "articles.json" => "posts#update_list", :as => :articles

  get "article/:id.json" => "posts#update_article", :as => :article

  get "result_check.json" => "posts#answer_check", :as => :result_check

  get "result_stats.json" => "posts#answer_stats", :as => :result_stats



  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
